#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QObject>
#include <QRadioButton>
#include <QButtonGroup>
#include <QLabel>
#include <QtNetwork/QtNetwork>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void Timer();

public Q_SLOTS:
    void InitFinished();
    void GetQRCodeFinished();
    void GetBoom();
    void GetBoomFinished();

private:
    QLabel* label;
    QNetworkAccessManager *manager;
    QNetworkAccessManager *manager_thread;
    QString id;
    QString url;
};

#endif // MAINWINDOW_H
