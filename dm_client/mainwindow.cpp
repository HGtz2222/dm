#include <thread>
#include <chrono>

#include "mainwindow.h"
#include "danmu.hpp"
// config.h 中只需要定义一个宏, 表示要访问的服务器 url 即可. 形如:
// #define URL "http://1.2.3.4:5678"
#include "config.h"

void ThreadEntry(MainWindow* main_window) {
    while (true) {
        emit main_window->Timer();
        std::chrono::milliseconds dura(1000);
        std::this_thread::sleep_for(dura);
    }
}

MainWindow::MainWindow(QWidget *parent): url(URL)
{
    (void)parent;
    int width = 370;
    int height = 370;

    this->resize(width, height);
    this->setWindowTitle("弹幕客户端");
    this->setAttribute(Qt::WA_DeleteOnClose, true);

    this->label = new QLabel();
    this->label->setParent(this);
    this->label->resize(width, height);
    // this->label->move(20, 20);
    this->label->setText(QString("二维码加载中..."));
    this->label->setAlignment(Qt::AlignCenter);
    this->label->show();

    // 调用服务器的初始化方法, 获取二维码
    manager = new QNetworkAccessManager();
    QNetworkReply* init_reply = manager->get(QNetworkRequest(QUrl(url + "/init")));
    connect(init_reply,SIGNAL(finished()), this, SLOT(InitFinished()));

    // 初始化第二个 Manager
    manager_thread = new QNetworkAccessManager();

    // 通过一个自定义信号 Timer 完成定时触发任务
    connect(this, SIGNAL(Timer()), this, SLOT(GetBoom()));
}

MainWindow::~MainWindow()
{
    delete this->label;
    delete manager;
}

void MainWindow::InitFinished() {
    QNetworkReply* init_reply = qobject_cast<QNetworkReply*>(sender());
    // 先获取 id
    this->id = init_reply->readAll();
    qDebug() << "id: " << this->id;
    init_reply->deleteLater();

    // 再获取 qrcode 图片
    QNetworkReply* qrcode_reply = manager->get(QNetworkRequest(QUrl(url + "/static/qrcode/" + this->id + ".png")));
    connect(qrcode_reply,SIGNAL(finished()), this, SLOT(GetQRCodeFinished()));

    // 创建一个线程完成定时拉取任务
    std::thread t(ThreadEntry, this);
    t.detach();
}

void MainWindow::GetQRCodeFinished() {
    QNetworkReply* qrcode_reply = qobject_cast<QNetworkReply*>(sender());
    QString file_name = "./qrcode" + this->id + ".png";
    QFile file(file_name);
    if (qrcode_reply->error() == QNetworkReply::NoError
            && file.open(QIODevice::WriteOnly)) {
        file.write(qrcode_reply->readAll());
        file.close();
        qDebug() << "get qrcode ok";
        // 显示图片
        QPixmap pixmap(file_name);
        this->label->setPixmap(pixmap);
    } else {
        qDebug() << "file open error";
        this->label->setText("二维码加载失败!");
    }
    qrcode_reply->deleteLater();
}

void MainWindow::GetBoom() {
    QNetworkReply* boom_reply = manager_thread->get(QNetworkRequest(QUrl(url + "/getmsg/" + this->id)));
    connect(boom_reply, SIGNAL(finished()), this, SLOT(GetBoomFinished()));
}

void MainWindow::GetBoomFinished() {
    QNetworkReply* boom_reply = qobject_cast<QNetworkReply*>(sender());
    QString boom = boom_reply->readAll();
    // qDebug() << "boom: " << boom;
    QStringList boom_list = boom.split("\n");
    for (QStringList::iterator it = boom_list.begin(); it != boom_list.end(); ++it) {
        if (*it == "") {
            continue;
        }
        CLabel *label = new CLabel(*it, this);
        (void)label;
    }
    boom_reply->deleteLater();
}
