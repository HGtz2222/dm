#ifndef DANMU_HPP
#define DANMU_HPP
#include <QWidget>
#include <QLabel>
#include <QPropertyAnimation>
#include <QApplication>
#include <QDesktopWidget>
#include <QColor>


class CLabel : public QLabel {
public:
    explicit CLabel(QString text, QWidget *parent = nullptr, QString color = "Red", int rtime = 5000, int height = 40) : QLabel(parent)
    {

        int text_width = text.size() * height;
        int text_height = height;

        int screen_width = QApplication::desktop()->width();
        int screen_height = QApplication::desktop()->height();

        int start_pos_x = screen_width;
        int start_pos_y = (qrand() % (screen_height - 2 * text_height)) + text_height;

        int end_pos_x = 0 - text_width;
        int end_pos_y = start_pos_y;

        QFont font("SimHei", height, QFont::Bold, false);
        font.setPixelSize(height);

        QPalette pe;
        pe.setColor(QPalette::WindowText,this->getColor(color));

        this->setText(text);
        this->setAlignment(Qt::AlignCenter);//居中
        this->resize(text_width, text_height);
        this->setFont(font);
        this->setPalette(pe);
        this->setWindowOpacity(1);
        this->setAutoFillBackground(true);
        this->setAttribute(Qt::WA_TranslucentBackground,true);
        this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool | Qt::WindowStaysOnTopHint);
        this->setAttribute(Qt::WA_DeleteOnClose, true);
        this->show();

        _animation=new QPropertyAnimation(this,"geometry",this);
        _animation->setDuration(rtime);
        _animation->setKeyValueAt(0, QRect(start_pos_x, start_pos_y , text_width, text_height));
        _animation->setKeyValueAt(1, QRect(end_pos_x, end_pos_y, text_width, text_height));
        _animation->start();
        QObject::connect(_animation, SIGNAL(finished()), this, SLOT(close()));
    }
    ~CLabel() {
        delete this->_animation;
    }
    QColor getColor(QString color) {
        if(color == "White"){
            return QColor(255,255,246,255);
        }else if(color =="Red"){
            return QColor(231,0,18,255);
        }else if(color =="Yellow"){
            return QColor(254,241,2,255);
        }else if(color == "Green"){
            return QColor(0,152,67,255);
        }else if(color == "Blue"){
            return QColor(0,160,234,255);
        }else if(color == "Pink"){
            return QColor(226,2,127,255);
        }else if(color == "Grass"){
            return QColor(144,195,32,255);
        }else if(color == "DBlue"){
            return QColor(0,46,114,255);
        }else if(color == "DYellow"){
            return QColor(240,171,42,255);
        }else if(color =="DPurple"){
            return QColor(104,58,123,255);
        }else if(color == "LBlue"){
            return QColor(129,193,205,255);
        }else if(color =="Brown"){
            return QColor(149,119,57,255);
        }else{
            return QColor(231,0,18,255);
        }
    }
protected:
private:
    QPropertyAnimation *_animation; //动画
};

#endif // DANMU_HPP
