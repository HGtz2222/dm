#include "mainwindow.h"
#include <QApplication>
#include <QTime>
#include "danmu.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
    MainWindow w;
    w.show();
    //CLabel *label = new CLabel("雷猴啊~");
    return a.exec();
}
