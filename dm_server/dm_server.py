from flask import Flask, render_template, request, make_response
import time
import qrcode
import logging

doc_msg = """
服务器地址:
    http://47.98.116.42:9091
    例如 http://47.98.116.42:9091/init 就是调用初始化接口

支持的接口：
1) /init (教师端启动时调用)
    服务器分配一个 session id， 返回给客户端.
    客户端根据 session_id 生成二维码
2) /client/[session_id] (教师端生成的二维码里包含这个 url)
    获取到对应的学生客户端(网页版本)
3) /getmsg/[session_id] (教师端每隔一秒调用一次这个接口, 获取当前的弹幕数据)
    教师端获取到弹幕数据
    返回结果是一个字符串, 包含若干行, 每行是一条弹幕
4) /putmsg/[session_id] (学生端写入弹幕数据, 教师端不需要关注)
    学生端写入弹幕数据
5) /static/qrcode/[session_id].png 获取图片
"""

url_base = "http://47.98.116.42:9091/client/"
global_session_base = 0
msg_queue = {}

app = Flask("boom_server")

def init_log():
    """
    初始化日志模块
    """
    logging.basicConfig(filename="./dm_server.log", format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


@app.route("/init")
def init():
    """
    分配 session_id 给客户端
    并创建一个队列
    """
    global global_session_base
    global_session_base += 1
    # 分配一个 session_id
    session_id = str(int(time.time())) + '_' + str(global_session_base)
    # 根据这个 session_id 创建一个队列. 超时时间暂时先不管
    msg_queue[session_id] = []
    # 创建一个二维码图片
    img = qrcode.make(url_base + session_id)
    img.save("./static/qrcode/" + session_id + ".png")
    # print("[access init]\t" + session_id)
    app.logger.info("[access init]\t" + session_id)
    return session_id


@app.route("/client/<session_id>")
def client(session_id):
    return render_template('client.html', session_id=session_id)


@app.route("/getmsg/<session_id>")
def get_msg(session_id):
    # 获取到多条弹幕, 每条弹幕占一行
    global msg_queue
    q = msg_queue[session_id]
    if len(q) > 0:
        # 限制日志不是每次 get 都打. 要不就太频繁了
        # print("[access get]\t" + session_id + '\t' + '\t'.join(q))
        app.logger.info("[access get]\t" + session_id + '\t' + '\t'.join(q))
        result = '\n'.join(q)
        msg_queue[session_id] = []
        return make_response(result, 200)
    else:
        return make_response("", 200)


@app.route("/putmsg/<session_id>", methods=['POST'])
def put_msg(session_id):
    msg = request.form.get("msg", "")
    if not msg:
        # print("[error]\t" + session_id + "\tmsg send failed")
        app.logger.error("[error]\t" + session_id + "\tmsg send failed")
        return "发送失败! session_id =" + session_id
    global msg_queue
    msg_queue[session_id].append(msg)
    # print("[access put]\t" + session_id + "\t" + msg)
    app.logger.info("[access put]\t" + session_id + "\t" + msg)
    return "发送成功!"


@app.route("/doc")
def get_doc():
    return doc_msg

if __name__ == "__main__":
    init_log()
    app.run(host="0.0.0.0", port=9091, debug=True)

