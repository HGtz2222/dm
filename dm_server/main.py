from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from dm_server import app
from dm_server import init_log

init_log()
http_server = HTTPServer(WSGIContainer(app))
http_server.listen(9091)
IOLoop.instance().start()
